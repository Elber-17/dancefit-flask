import os

from flask import Flask
from flask import request
from flask import jsonify
from flask_cors import CORS
from flask_jwt_extended import JWTManager
from flask_jwt_extended import jwt_required
from flask_jwt_extended import get_jwt_identity
from flask_migrate import Migrate
from dotenv import load_dotenv

from core.sqlalchemy import db
from config.db import URI
from resources.user import User
from resources.session import Session
from resources.course import Course
from errors.token_errors import TokenErrorHandler
from models.course import *
from models.user_course import *

from validations.login import validate_login
from validations.user import validate_user
from validations.user import validate_patch

load_dotenv(verbose=True)

app = Flask(__name__)

app.config['SQLALCHEMY_DATABASE_URI'] = URI
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['JWT_SECRET_KEY'] = os.getenv('JWT_SECRET')
app.config['JWT_COOKIE_SECURE'] = False
app.config['JWT_COOKIE_SAMESITE'] = 'Lax'
app.config['JWT_ACCESS_TOKEN_EXPIRES'] = 24*3600
app.config['JWT_TOKEN_LOCATION'] = ['cookies']
app.config['JWT_COOKIE_CSRF_PROTECT'] = True

db.init_app(app)
migrate = Migrate(app, db)
CORS(app, supports_credentials=True)
jwt = JWTManager(app)

jwt.revoked_token_loader(TokenErrorHandler.revoken_token)
jwt.unauthorized_loader(TokenErrorHandler.unauthorized_token)
jwt.expired_token_loader(TokenErrorHandler.expired_token)

@app.route('/user/', methods=['GET', 'POST', 'PATCH', 'DELETE'])
@validate_user
def user(body):
    user = User()

    if request.method == 'GET':
        return user.get()
    
    if request.method == 'POST':
        return user.post(body)
    
    if request.method == 'PATCH':
        return user.patch(body)
    
    if request.method == 'DELETE':
        return user.delete()

@app.route('/session/login/', methods=['POST'])
@validate_login
def login(username, password):
    session = Session()

    return session.login(username, password)

@app.route('/session/check/', methods=['GET'])
@jwt_required
def check_session():
    session = Session()

    return session.check()

@app.route('/session/logout/', methods=['DELETE'])
def logout():
    session = Session()

    return session.logout()


@app.route('/course/', methods=['GET'])
def course():
    course = Course()

    return course.get()

@app.route('/course/<id>/image', methods=['GET'])
def course_image():
    course = Course()

    return course.get()

if __name__ == '__main__':
	app.run(use_debugger=True, host=os.getenv('HOST') , port=8888, use_reloader=True)
