from functools import wraps
from flask import request
from flask.wrappers import Response

from core.resource import BaseResource

baseResource = BaseResource()

def validate_post():
    body = request.get_json()
    baseResource = BaseResource()
    registrable_fields = ['name', 'last_name', 'email', 'password']

    for key in body.keys():
        if key not in registrable_fields:
            return baseResource.make_response({'Bad Request': f"El parametro '{key}' no es un campo registrable."}, 400)

    for value in body.values():
        if not value:
            return baseResource.make_response({'Bad Request': f"El parametro '{key}' no puede estar vacío."}, 400)

    if(not body):
        return baseResource.make_response({'Bad Request': 'Debes mandar los parametros del usuario que deseas registrar, que son el nombre, el correo y la contraseña.'}, 400)

    if(not body.get('name', False)):
        return baseResource.make_response({'Bad Request': 'Debes mandar el nombre'}, 400)

    if(not body.get('email', False)):
        return baseResource.make_response({'Bad Request': 'Debes mandar el email'}, 400)
    
    if(not body.get('password', False)):
        return baseResource.make_response({'Bad Request': 'Debes mandar la contraseña'}, 400)

    return body

def validate_patch():
    body = request.get_json()
    baseResource = BaseResource()
    updatable_fields = ['name', 'last_name', 'cedula', 'phone']

    if(not body):
        return baseResource.make_response({'Bad Request': 'No has mandado parametros para actualizar'}, 400)

    for key in body.keys():
        if key not in updatable_fields:
            return baseResource.make_response({'Bad Request': f"El parametro '{key}' no es un campo actualizable válido."}, 400)

    for value in body.values():
        if not value:
            return baseResource.make_response({'Bad Request': f"El parametro '{key}' no puede estar vacío."}, 400)

    return body

def validate_user(f):
    @wraps(f)
    def decorated_function():
        body = {}

        if request.method == 'GET':
            pass
    
        if request.method == 'POST':
            body = validate_post()

        if request.method == 'PATCH':
            body = validate_patch()
        
        if request.method == 'DELETE':
            pass
        
        if type(body) == Response:
            return body

        return f(body)

    return decorated_function