from functools import wraps
from flask import request

from core.resource import BaseResource

def validate_login(f):
    @wraps(f)
    def decorated_function():
        body = request.get_json();
        baseResource = BaseResource()

        if(not body):
            return baseResource.make_response({'Bad Request': 'Debes mandar correo y contraseña'}, 400)

        if(not body.get('email', False)):
            return baseResource.make_response({'Bad Request': 'Debes mandar el email'}, 400)
        
        if(not body.get('password', False)):
            return baseResource.make_response({'Bad Request': 'Debes mandar la contraseña'}, 400)

        return f(body['email'], body['password'])

    return decorated_function