import datetime

from sqlalchemy import Column
from sqlalchemy.dialects.mysql import TINYINT
from sqlalchemy.dialects.mysql import BIGINT
from sqlalchemy.dialects.mysql import VARCHAR
from sqlalchemy.dialects.mysql import TIMESTAMP

from core.sqlalchemy import db

class User(db.Model):
    __tablename__ = 'user'

    _id_ = Column('id', BIGINT(unsigned=True), primary_key=True)
    name = Column(VARCHAR(64))
    last_name = Column(VARCHAR(64))
    cedula = Column(VARCHAR(16))
    phone = Column(VARCHAR(16))
    password = Column(VARCHAR(1024))
    email = Column(VARCHAR(128), unique=True)
    status_id = Column(TINYINT(unsigned=True), db.ForeignKey('user_status.id'))
    type_id = Column(TINYINT(unsigned=True), db.ForeignKey('user_type.id'))
    creation_date = Column(TIMESTAMP(), default=datetime.datetime.utcnow)

    status = db.relationship('UserStatus')
    type = db.relationship('UserType')

class UserStatus(db.Model):
    __tablename__ = 'user_status'

    _id_ = Column('id', TINYINT(unsigned=True), primary_key=True, autoincrement=False)
    code = Column(VARCHAR(32))
    description = Column(VARCHAR(128))

class UserType(db.Model):
    __tablename__ = 'user_type'

    _id_ = Column('id', TINYINT(unsigned=True), primary_key=True, autoincrement=False)
    code = Column(VARCHAR(32))
    description = Column(VARCHAR(128))

