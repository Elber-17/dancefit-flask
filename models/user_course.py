from sqlalchemy import Column
from sqlalchemy.dialects.mysql import INTEGER
from sqlalchemy.dialects.mysql import TINYINT
from sqlalchemy.dialects.mysql import BIGINT
from sqlalchemy.dialects.mysql import VARCHAR
from sqlalchemy.dialects.mysql import DATETIME

from core.sqlalchemy import db
from models.user import User
from models.course import Course
from models.course import CourseRegistry

#Tabla relacion
class UserCourse(db.Model):
    __tablename__ = 'user_course'

    _id_ = Column('id', BIGINT(unsigned=True), primary_key=True)
    expiration_date = Column(DATETIME())
    user_id = Column(BIGINT(unsigned=True), db.ForeignKey('user.id'))
    course_id = Column(INTEGER(unsigned=True), db.ForeignKey('course.id'))
    status_id = Column(TINYINT(unsigned=True), db.ForeignKey('user_course_status.id'))
    registry_id = Column(BIGINT(unsigned=True), db.ForeignKey('course_registry.id'))

    completion_percentage = Column(VARCHAR(4))

    user = db.relationship(User)
    course = db.relationship(Course)
    status = db.relationship('UserCourseStatus')
    registry = db.relationship(CourseRegistry)

class UserCourseStatus(db.Model):
    __tablename__ = 'user_course_status'

    _id_ = Column('id', TINYINT(unsigned=True), primary_key=True, autoincrement=False)
    code = Column(VARCHAR(32))
    description = Column(VARCHAR(128))