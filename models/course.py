from sqlalchemy import Column
from sqlalchemy.dialects.mysql import TINYINT
from sqlalchemy.dialects.mysql import INTEGER
from sqlalchemy.dialects.mysql import VARCHAR
from sqlalchemy.dialects.mysql import SMALLINT
from sqlalchemy.dialects.mysql import BIGINT

from core.sqlalchemy import db

class Course(db.Model):
    __tablename__ = 'course'

    _id_ = Column('id', INTEGER(unsigned=True), primary_key=True)
    name = Column(VARCHAR(128))
    description = Column(VARCHAR(1024))
    type_id = Column(TINYINT(unsigned=True), db.ForeignKey('course_type.id'))
    category_id = Column(SMALLINT(unsigned=True), db.ForeignKey('course_category.id'))

    _type_ = db.relationship('CourseType')
    category = db.relationship('CourseCategory')


class CourseType(db.Model):
    __tablename__ = 'course_type'

    _id_ = Column('id', TINYINT(unsigned=True), primary_key=True, autoincrement=False)
    name = Column(VARCHAR(128))
    description = Column(VARCHAR(1024))

class CourseCategory(db.Model):
    __tablename__ = 'course_category'

    _id_ = Column('id', SMALLINT(unsigned=True), primary_key=True, autoincrement=False)
    name = Column(VARCHAR(128))
    description = Column(VARCHAR(5120))


class CourseRegistry(db.Model):
    __tablename__ = 'course_registry'

    _id_ = Column('id', BIGINT(unsigned=True), primary_key=True)
    img_url = Column(VARCHAR(5120))    
    status_id = Column(TINYINT(unsigned=True), db.ForeignKey('course_registry_status.id'))

    status = db.relationship('CourseRegistryStatus')

class CourseRegistryStatus(db.Model):
    __tablename__ = 'course_registry_status'

    _id_ = Column('id', TINYINT(unsigned=True), primary_key=True, autoincrement=False)
    code = Column(VARCHAR(64))
    description = Column(VARCHAR(128))

class CourseImage(db.Model):
    __tablename__ = 'course_image'
 
    _id_ = Column('id', TINYINT(unsigned=True), primary_key=True, autoincrement=False)
    course_id = Column(INTEGER(unsigned=True), db.ForeignKey('course.id'))
    path_url = Column(VARCHAR(1024))
    _type_ = Column('type' ,VARCHAR(1024))

    category = db.relationship(Course)