"""volcado de datos

Revision ID: b584bc9e2825
Revises: 8e54479e4501
Create Date: 2020-10-18 20:21:36.544859

"""
from alembic import op
import sqlalchemy as sa

from core.sqlalchemy import db
from models.user import UserStatus
from models.user import UserType
from models.course import Course
from models.course import CourseCategory
from models.course import CourseType
from models.course import CourseRegistryStatus
from models.user_course import UserCourseStatus
from models.course import CourseImage


# revision identifiers, used by Alembic.
revision = 'b584bc9e2825'
down_revision = '905b4dbb1584'
branch_labels = None
depends_on = None


def save_data(data, is_array=True):
    if(is_array):
        for elem in data:
            db.session.add(elem)
    else:
        db.session.add(data)

    db.session.commit()

    return

def user_status():
    user_status_active = UserStatus()
    user_status_delete = UserStatus()

    user_status_active._id_ = 1
    user_status_active.code = 'Activo'
    user_status_active.description = 'Usuario activo del sistema'

    user_status_delete._id_ = 2
    user_status_delete.code = 'Eliminado'
    user_status_delete.description = 'Usuario eliminado del sistema'

    statuses = [user_status_active, user_status_delete]

    save_data(statuses)

    return

def user_type():
    user_type_admin = UserType()
    user_type_teacher = UserType()
    user_type_student = UserType()

    user_type_admin._id_ = 1;
    user_type_admin.code = 'Administrador'
    user_type_admin.description = 'Usuario Administrador de la plataforma, acceso a todos los modulos del sistema'

    user_type_teacher._id_ = 2;
    user_type_teacher.code = 'Profesor'
    user_type_teacher.description = 'Usuario registrado como profesor de la plataforma, con permisos de acceso al modulo de profesor'

    user_type_student._id_ = 3;
    user_type_student.code = 'Estudiante'
    user_type_student.description = 'Usuario registrado como estudiante de la plataforma, solo puede acceder a cursos y comprarlos'

    types = [user_type_admin, user_type_teacher, user_type_student]

    save_data(types)

    return

def user_course_status():
    status_1 = UserCourseStatus()
    status_2 = UserCourseStatus()

    status_1._id_ = 1
    status_1.code = 'Disponible'
    status_1.description = 'Curso disponible para que los usuarios lo puedan ver'

    status_2._id_ = 2
    status_2.code = 'Inhabilitado'
    status_2.description = 'Curso no disponible en la plataforma'

    save_data([status_1, status_2])

    return


def course_registry_status():
    registry_status_cofirm = CourseRegistryStatus()
    registry_status_processing = CourseRegistryStatus()

    registry_status_cofirm._id_ = 1
    registry_status_cofirm.code = 'Registro Confirmado'
    registry_status_cofirm.description = 'Registro procesado con éxito la inscripción del usuario en el curso'

    registry_status_processing._id_ = 2
    registry_status_processing.code = 'Registro en proceso de confirmación'
    registry_status_processing.description = 'Registro a la espera de ser confirmado para que el usuario pueda acceder al curso comprado'

    save_data([registry_status_cofirm, registry_status_processing])

    return

def course_type():
    type_1 = CourseType()
    type_2 = CourseType()
    type_3 = CourseType()

    type_1._id_ = 1
    type_1.name = 'Premium'
    type_1.description = 'Curso que solo lo pueden ver personas que hayan pagado'

    type_2._id_ = 2
    type_2.name = 'Registrados'
    type_2.description = 'Curso que lo pueden ver personas que se hayan registrado en la plataforma'

    type_3._id_ = 3
    type_3.name = 'Libre'
    type_3.description = 'Curso que lo puede ver cualquier persona que ingrese en la página.'

    types = [type_1, type_2, type_3]

    save_data(types)

    return

def course():
    course_1 = Course()
    course_2 = Course()
    course_3 = Course()
    course_4 = Course()
    course_5 = Course()

    course_1._id_ = 1
    course_1.name = 'Gimmnasia'
    course_1.description = 'Práctica de ejercicio físico que sirve para desarrollar, fortalecer y dar flexibilidad al cuerpo'
    course_1.type_id = 3    

    course_2._id_ = 2
    course_2.name = 'Canto'
    course_2.description = 'Dominar la técnica vocal y proporcionarle al estudiante una visión amplia de los diferentes estilos musicales'
    course_2.type_id = 3

    course_3._id_ = 3
    course_3.name = 'Commercial Dance'
    course_3.description = 'Tendencia que busca involucrar cualquier estilo de baile con la era en que vivimos actualmente'
    course_3.type_id = 3

    course_4._id_ = 4
    course_4.name = 'Danza'
    course_4.description = 'Elementos técnicos de la danza clásica y moderna, desarrollando el ritmo, dominio corporal, creatividad y concentración del niño'
    course_4.type_id = 3

    course_5._id_ = 5
    course_5.name = 'Teatro'
    course_5.description = 'Desarrollar y potenciar las habilidades y destrezas corporales, gestuales y vocales'
    course_5.type_id = 3

    courses = [course_1, course_2, course_3, course_4, course_5]

    save_data(courses)

    return

def couse_image():
    course_image_gimmnasia = CourseImage()
    course_image_canto = CourseImage()
    course_image_commecial_dance = CourseImage()
    course_image_danza = CourseImage()
    course_image_teatro = CourseImage()

    course_image_gimmnasia._id_ = 1
    course_image_gimmnasia.course_id = 1
    course_image_gimmnasia.path_url = 'files/images/courses/img/Gimnasia.jpg'

    course_image_canto._id_ = 2
    course_image_canto.course_id = 2
    course_image_canto.path_url = 'files/images/courses/img/Canto.jpg'

    course_image_commecial_dance._id_ = 3
    course_image_commecial_dance.course_id = 3
    course_image_commecial_dance.path_url = 'files/images/courses/img/Commercialdance.jpg'

    course_image_danza._id_ = 4
    course_image_danza.course_id = 4
    course_image_danza.path_url = 'files/images/courses/img/Danza.jpg'

    course_image_teatro._id_ = 5
    course_image_teatro.course_id = 5
    course_image_teatro.path_url = 'files/images/courses/img/Teatro.jpg'

    course_images = [course_image_gimmnasia, course_image_canto, course_image_commecial_dance, course_image_danza, course_image_danza]

    save_data(course_images)

def course_category():
    pass


def upgrade():
    user_status()
    user_type()
    user_course_status()
    course_registry_status()
    course_type()
    course()
    course_category()
    couse_image()

    return

def downgrade():
    db.session.execute('SET FOREIGN_KEY_CHECKS = 0')

    db.session.execute('TRUNCATE TABLE user_status')
    db.session.execute('TRUNCATE TABLE user_type')
    db.session.execute('TRUNCATE TABLE user_course_status')
    db.session.execute('TRUNCATE TABLE course')
    db.session.execute('TRUNCATE TABLE course_type')
    db.session.execute('TRUNCATE TABLE course_registry_status')
    db.session.execute('TRUNCATE TABLE course_category')
    db.session.execute('TRUNCATE TABLE course_image')

    db.session.execute('SET FOREIGN_KEY_CHECKS = 1')

    return