import os

params = {
		'dialect' : os.getenv('DB_TYPE'),
		'driver' : os.getenv('DB_DRIVER'),
		'username' : os.getenv('DB_USER'),
		'password' : os.getenv('DB_PASSWORD'),
		'ip' : os.getenv('DB_HOST'),
		'port' : os.getenv('DB_PORT'),
		'database' : os.getenv('DB_NAME'),
		'charset' : 'utf8mb4',
		}

URI = '{dialect}+{d}://{u}:{p}@{ip}:{port}/{db}?charset={charset}&use_unicode=true&binary_prefix=true'
URI = URI.format(
	dialect=params['dialect'],
	d=params['driver'],
	u=params['username'],
	p=params['password'],
	ip=params['ip'],
	port=params['port'],
	db=params['database'],
	charset=params['charset']
)