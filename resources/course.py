from flask import request
from flask_jwt_extended import jwt_required
from flask_jwt_extended import get_jwt_identity

from core.resource import BaseResource
from core.sqlalchemy import db
from models.course import Course as ModelCourse

class Course(BaseResource):
    @jwt_required
    def get(self):
        user_id = get_jwt_identity()
        courses = ModelCourse.query.all()
        visible_attributes = ['_id_','name', 'description']

        response = [(dict((attr if attr != '_id_' else 'id', getattr(course, attr)) for attr in visible_attributes)) for course in courses]

        return self.make_response(response, 200)
    
    @jwt_required
    def post(self, body):
        return self.make_response({'status':'siuuu'}, 201)

    @jwt_required
    def patch(self, body):
        return self.make_response({'OK': 'Usuario actualizado con éxito'}, 200)

    @jwt_required
    def delete(self):
        return self.make_response({'OK': 'Usuario Eliminado'}, 200)