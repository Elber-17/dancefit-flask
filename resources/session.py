from json import dumps
from json import loads

from flask import request
from flask_jwt_extended import create_access_token
from flask_jwt_extended import set_access_cookies
from flask_jwt_extended import unset_jwt_cookies
from sqlalchemy import func
from bcrypt import checkpw

from core.resource import BaseResource
from models.user import User

class Session(BaseResource):
    def login(self, email, password):
        user = self._get_user_(email, password)

        if not user:
            return self.make_response({'Not Found' : 'El correo o contraseña son incorrectos'}, 404)

        response = self.make_response({
                    'Success' : 'User loged'
                })
        set_access_cookies(response, create_access_token(user._id_))

        response = self.addSameSiteNoneToCookieHeader(response)
        
        return self.make_response(response, 200)

    def _get_user_(self, username_or_email, password):
        user = User.query.filter_by(email=username_or_email).first()

        if(user):
            return user if checkpw(password.encode('UTF-8'), user.password.encode('UTF-8')) else False
        
        return False
    
    def check(self):
        return self.make_response({'checked' : 'token valido'}, 200)

    def logout(self):
        response = self.make_response({'OK': 'Session cerrada con éxito'})
        unset_jwt_cookies(response)

        response = self.addSameSiteNoneToCookieHeader(response)

        return self.make_response(response, 200)
    
    #Este metodo es para la version de python 3.7 que no añade la opcion Samesite=None
    #en la respuesta que envia
    def addSameSiteNoneToCookieHeader(self, response):
        cookies = response.headers.get_all('Set-Cookie')
        access_token_cookie = cookies[0]
        csrf_access_token = cookies[1]
        refresh_token_cookie = ''
        csrf_refresh_token = ''

        if not 'SameSite' in access_token_cookie:
            access_token_cookie += '; SameSite=None'

        if not 'SameSite' in csrf_access_token:
            csrf_access_token += '; SameSite=None'

        if len(cookies) > 2:
            refresh_token_cookie = cookies[2]
            csrf_refresh_token = cookies[3]

            if not 'SameSite' in refresh_token_cookie:
                refresh_token_cookie += '; SameSite=None'

            if not 'SameSite' in csrf_refresh_token:
                csrf_refresh_token += '; SameSite=None'
            
            response.headers.setlist('Set-Cookie', [access_token_cookie, csrf_access_token, refresh_token_cookie, csrf_refresh_token])

        else:
            response.headers.setlist('Set-Cookie', [access_token_cookie, csrf_access_token])
        
        if request.method == 'POST':
            data = loads(response.data)
            data['csrf_access_token'] = csrf_access_token[csrf_access_token.find('=')+1:csrf_access_token.find(';')]
            response.data = dumps(data)

        return response