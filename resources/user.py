from flask import request
from flask_jwt_extended import jwt_required
from flask_jwt_extended import get_jwt_identity
from bcrypt import hashpw
from bcrypt import gensalt

from core.resource import BaseResource
from core.sqlalchemy import db
from models.user import User as ModelUser
from datetime import datetime

class User(BaseResource):
    @jwt_required
    def get(self):
        user_id = get_jwt_identity()
        user = ModelUser.query.get(user_id);
        visible_attributes = ['name', 'last_name', 'email', 'cedula', 'phone', 'creation_date', 'status', 'type']

        response = (dict((attr, getattr(user, attr)) for attr in visible_attributes))

        return self.make_response(response, 200)
    
    def post(self, body):
        name = body.get('name')
        last_name = body.get('last_name')
        password = hashpw(body.get('password').encode('UTF-8'), gensalt(12)) 
        email = body.get('email')
        creation_date = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        user = ModelUser(name=name, last_name=last_name, password=password, email=email, creation_date=creation_date)

        db.session.add(user)
        db.session.commit()
        response = {
            'Ok' : 'Usuario Registrado'
        }

        return self.make_response(response, 201)

    @jwt_required
    def patch(self, body):
        user_id = get_jwt_identity()
        user = ModelUser.query.get(user_id)

        for key, value in body.items():
            setattr(user, key, value)

        db.session.add(user)
        db.session.commit()

        return self.make_response({'OK': 'Usuario actualizado con éxito'}, 200)

    @jwt_required
    def delete(self):
        user_id = get_jwt_identity()
        user = ModelUser.query.get(user_id)
        
        db.session.add(user)
        db.session.commit()

        return self.make_response({}, 204)